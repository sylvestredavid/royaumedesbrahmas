const form = document.querySelector(".formulaire")
const nom = document.getElementById("nom")
const nomError = document.getElementById("nom_error")
const prenom = document.getElementById("prenom")
const prenomError = document.getElementById("prenom_error")
const email = document.getElementById("email")
const emailError = document.getElementById("email_error")
const phone = document.getElementById("phone")
const phoneError = document.getElementById("phone_error")
const objet = document.getElementById("objet")
const objetError = document.getElementById("objet_error")
const message = document.getElementById("message")
const messageError = document.getElementById("message_error")

let loading = false

form.addEventListener("submit", e => {
    e.preventDefault()
    if(loading) return
    const nomIsOk = checkNom()
    const prenomIsOk = checkPrenom()
    const emailIsOk = checkEmail()
    const objetIsOk = checkObjet()
    const messageIsOk = checkMessage()
    const phoneIsOk = checkPhone()
    if(!nomIsOk || !prenomIsOk || !emailIsOk || !objetIsOk || !messageIsOk || !phoneIsOk) {
        document.getElementById("result").innerText = "Le formulaire contient des erreurs"
        document.getElementById("result").classList.add("error")
        return
    }
    document.getElementById("result").innerText = ""
    document.getElementById("result").classList.remove("error")
    setLoading(true)
    checkNom()
    const body = {
        name: `${prenom.value}${nom.value}`,
        email: email.value,
        contact: phone.value,
        objet: objet.value,
        message: message.value
    }
    console.log(body)
    fetch(
        '../php/mail.php',
        {
            method: "POST",
            body: JSON.stringify(body)
        }
    )
        .then(response => response.json())
        .then(res => {
            if(res.success) {
                document.getElementById("result").innerText = "Votre message à bien été envoyé"
                nom.value = ""
                prenom.value = ""
                email.value = ""
                phone.value = ""
                objet.value = ""
                message.value = ""
            }
        })
        .catch(error => console.log(error))
        .finally(() => setLoading(false))
})

function checkNom() {
    if(nom.value.length === 0) {
        nom.classList.add("input_error")
        nomError.innerText = "Le nom est requis"
        return false
    }
    nom.classList.remove("input_error")
    nomError.innerText = ""
    return true
}

function checkPrenom() {
    if(prenom.value.length === 0) {
        prenom.classList.add("input_error")
        prenomError.innerText = "Le prenom est requis"
        return false
    }
    prenom.classList.remove("input_error")
    prenomError.innerText = ""
    return true
}

function checkEmail() {
    if(email.value.length === 0) {
        email.classList.add("input_error")
        emailError.innerText = "L'adresse email est requis"
        return false
    }
    if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value)) {
        email.classList.add("input_error")
        emailError.innerText = "L'adresse email n'est pas au bon format"
        return false
    }
    email.classList.remove("input_error")
    emailError.innerText = ""
    return true
}

function checkPhone() {
    if(phone.value.length > 0 && !/^([0-9]){10}$/.test(phone.value)) {
        phone.classList.add("input_error")
        phoneError.innerText = "Le numéro de téléphone n'est pas au bon format"
        return false
    }
    phone.classList.remove("input_error")
    phoneError.innerText = ""
    return true
}

function checkObjet() {
    if(objet.value.length === 0) {
        objet.classList.add("input_error")
        objetError.innerText = "L'objet est requis"
        return false
    }
    objet.classList.remove("input_error")
    objetError.innerText = ""
    return true
}

function checkMessage() {
    if(message.value.length === 0) {
        message.classList.add("input_error")
        messageError.innerText = "Le message est requis"
        return false
    }
    message.classList.remove("input_error")
    messageError.innerText = ""
    return true
}

function setLoading(val) {
    const submit = document.getElementById("submit")
    const loadingBtn = document.getElementById("loading")
    loading = val
    if(loading) {
        submit.style.display = "none"
        loadingBtn.style.display = "block"
    } else {
        submit.style.display = "block"
        loadingBtn.style.display = "none"
    }
}