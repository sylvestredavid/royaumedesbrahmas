const openMenu = document.getElementById("open_menu")
const closeMenu = document.getElementById("close_menu")
const menuMobile = document.getElementById("menu_mobile")

openMenu.addEventListener("click", () => {
    menuMobile.classList.add("openMenu")
})

closeMenu.addEventListener("click", () => {
    menuMobile.classList.remove("openMenu")
})

const carroussels = document.getElementsByClassName("main_section_imagesContainer")
console.log(window.innerWidth)
for (let i = 0; i < carroussels.length; i++) {
    const arrowLeft = carroussels[i].querySelector(".chevron_left")
    const arrowRight = carroussels[i].querySelector(".chevron_right")
    const cursors = carroussels[i].getElementsByClassName("cursor")
    const imageContainer = carroussels[i].getElementsByClassName("main_section_imagesContainer_images")[0]
    const nbImage = cursors.length
    let currentImage = 0;

    arrowLeft.addEventListener("click", () => {
        currentImage = currentImage > 0 ? currentImage - 1 : 0
        updateImage(currentImage, cursors, imageContainer)
    })

    arrowRight.addEventListener("click", () => {
        currentImage = currentImage < nbImage - 1 ? currentImage + 1 : nbImage - 1
        updateImage(currentImage, cursors, imageContainer)
    })
}

function updateImage(currentImage, cursors, imageContainer) {
    const left = window.innerWidth > 992 ?  -496 * currentImage : -375 * currentImage
    imageContainer.style.left = `${left}px`
    for (let k = 0; k < cursors.length; k++) {
        if (k === currentImage) {
            cursors[k].classList.add("currentImage")
        } else {
            cursors[k].classList.remove("currentImage")
        }
    }
}